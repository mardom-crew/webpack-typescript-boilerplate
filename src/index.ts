import { Product } from "./product.interface";
import './style.scss';

const product: Product = { id: 123, name: "Hello Typescript and Webpack" };

function draw() {
    const el: HTMLDivElement = document.createElement('div');
    const btn: HTMLButtonElement = document.createElement('button');

    el.innerHTML = product.name;
    el.classList.add('title');

    return el;
}

document.body.appendChild(draw());