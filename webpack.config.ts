import * as webpack from 'webpack';
import * as TerserPlugin from "terser-webpack-plugin";
import * as path from 'path';
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import * as HtmlWebpackPlugin from "html-webpack-plugin";

const Config: webpack.Configuration = ({
  devServer: {
      contentBase: "/dist"
  },
  entry: path.resolve(process.cwd(), "src", "index.ts"),
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: /node_modules/
      },
      {
        test: /\.s?css$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
    ]
  },
  resolve: {
    extensions: [".ts", ".js"]
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(process.cwd(), "dist")
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({ template: './src/index.html' })
  ],
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        cache: false
      })
    ]
  }
});

export default Config;